﻿namespace TrucklerSampleApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtbxDiscordSnowflake = new System.Windows.Forms.TextBox();
            this.lblDiscordSnowflake = new System.Windows.Forms.Label();
            this.txtbxSteamID = new System.Windows.Forms.TextBox();
            this.lblSteamId = new System.Windows.Forms.Label();
            this.chkbxConnected = new System.Windows.Forms.CheckBox();
            this.lblConnected = new System.Windows.Forms.Label();
            this.txtbxLog = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGetTrailers = new System.Windows.Forms.Button();
            this.btnGetTruck = new System.Windows.Forms.Button();
            this.btnGetJob = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbxHistoryMessage = new System.Windows.Forms.TextBox();
            this.lstbxHistory = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(427, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "This is a sample windows forms app to demonstrate how to connect to Truckler!";
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(9, 28);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 1;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtbxDiscordSnowflake);
            this.groupBox1.Controls.Add(this.lblDiscordSnowflake);
            this.groupBox1.Controls.Add(this.txtbxSteamID);
            this.groupBox1.Controls.Add(this.lblSteamId);
            this.groupBox1.Controls.Add(this.chkbxConnected);
            this.groupBox1.Controls.Add(this.lblConnected);
            this.groupBox1.Location = new System.Drawing.Point(3, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(395, 100);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Status";
            // 
            // txtbxDiscordSnowflake
            // 
            this.txtbxDiscordSnowflake.Location = new System.Drawing.Point(122, 63);
            this.txtbxDiscordSnowflake.Name = "txtbxDiscordSnowflake";
            this.txtbxDiscordSnowflake.ReadOnly = true;
            this.txtbxDiscordSnowflake.Size = new System.Drawing.Size(267, 23);
            this.txtbxDiscordSnowflake.TabIndex = 6;
            // 
            // lblDiscordSnowflake
            // 
            this.lblDiscordSnowflake.AutoSize = true;
            this.lblDiscordSnowflake.Location = new System.Drawing.Point(6, 66);
            this.lblDiscordSnowflake.Name = "lblDiscordSnowflake";
            this.lblDiscordSnowflake.Size = new System.Drawing.Size(110, 15);
            this.lblDiscordSnowflake.TabIndex = 5;
            this.lblDiscordSnowflake.Text = "Discord Snowflake: ";
            // 
            // txtbxSteamID
            // 
            this.txtbxSteamID.Location = new System.Drawing.Point(69, 34);
            this.txtbxSteamID.Name = "txtbxSteamID";
            this.txtbxSteamID.ReadOnly = true;
            this.txtbxSteamID.Size = new System.Drawing.Size(320, 23);
            this.txtbxSteamID.TabIndex = 4;
            // 
            // lblSteamId
            // 
            this.lblSteamId.AutoSize = true;
            this.lblSteamId.Location = new System.Drawing.Point(6, 37);
            this.lblSteamId.Name = "lblSteamId";
            this.lblSteamId.Size = new System.Drawing.Size(57, 15);
            this.lblSteamId.TabIndex = 3;
            this.lblSteamId.Text = "Steam ID:";
            // 
            // chkbxConnected
            // 
            this.chkbxConnected.AutoSize = true;
            this.chkbxConnected.Enabled = false;
            this.chkbxConnected.Location = new System.Drawing.Point(80, 20);
            this.chkbxConnected.Name = "chkbxConnected";
            this.chkbxConnected.Size = new System.Drawing.Size(15, 14);
            this.chkbxConnected.TabIndex = 3;
            this.chkbxConnected.UseVisualStyleBackColor = true;
            // 
            // lblConnected
            // 
            this.lblConnected.AutoSize = true;
            this.lblConnected.Location = new System.Drawing.Point(6, 19);
            this.lblConnected.Name = "lblConnected";
            this.lblConnected.Size = new System.Drawing.Size(68, 15);
            this.lblConnected.TabIndex = 3;
            this.lblConnected.Text = "Connected:";
            // 
            // txtbxLog
            // 
            this.txtbxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtbxLog.Location = new System.Drawing.Point(3, 19);
            this.txtbxLog.Multiline = true;
            this.txtbxLog.Name = "txtbxLog";
            this.txtbxLog.ReadOnly = true;
            this.txtbxLog.Size = new System.Drawing.Size(372, 360);
            this.txtbxLog.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtbxLog);
            this.groupBox2.Location = new System.Drawing.Point(404, 57);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(378, 382);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Client Log";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(799, 478);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(791, 450);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Main";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnGetTrailers);
            this.panel1.Controls.Add(this.btnGetTruck);
            this.panel1.Controls.Add(this.btnGetJob);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btnConnect);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(785, 444);
            this.panel1.TabIndex = 5;
            // 
            // btnGetTrailers
            // 
            this.btnGetTrailers.Enabled = false;
            this.btnGetTrailers.Location = new System.Drawing.Point(165, 163);
            this.btnGetTrailers.Name = "btnGetTrailers";
            this.btnGetTrailers.Size = new System.Drawing.Size(87, 23);
            this.btnGetTrailers.TabIndex = 7;
            this.btnGetTrailers.Text = "Get Trailer(s)";
            this.btnGetTrailers.UseVisualStyleBackColor = true;
            this.btnGetTrailers.Click += new System.EventHandler(this.btnGetTrailers_Click);
            // 
            // btnGetTruck
            // 
            this.btnGetTruck.Enabled = false;
            this.btnGetTruck.Location = new System.Drawing.Point(84, 163);
            this.btnGetTruck.Name = "btnGetTruck";
            this.btnGetTruck.Size = new System.Drawing.Size(75, 23);
            this.btnGetTruck.TabIndex = 6;
            this.btnGetTruck.Text = "Get Truck";
            this.btnGetTruck.UseVisualStyleBackColor = true;
            this.btnGetTruck.Click += new System.EventHandler(this.btnGetTruck_Click);
            // 
            // btnGetJob
            // 
            this.btnGetJob.Enabled = false;
            this.btnGetJob.Location = new System.Drawing.Point(3, 163);
            this.btnGetJob.Name = "btnGetJob";
            this.btnGetJob.Size = new System.Drawing.Size(75, 23);
            this.btnGetJob.TabIndex = 5;
            this.btnGetJob.Text = "Get Job";
            this.btnGetJob.UseVisualStyleBackColor = true;
            this.btnGetJob.Click += new System.EventHandler(this.btnGetJob_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.txtbxHistoryMessage);
            this.tabPage2.Controls.Add(this.lstbxHistory);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(791, 450);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Message History";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(603, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "The json is already parsed into the event details data structure and can be used " +
    "directly as shown on the main tab!";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(665, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "This tab shows message history and the json of the event data for the selected ev" +
    "ent. In the event details of the event handler.";
            // 
            // txtbxHistoryMessage
            // 
            this.txtbxHistoryMessage.Location = new System.Drawing.Point(134, 36);
            this.txtbxHistoryMessage.Multiline = true;
            this.txtbxHistoryMessage.Name = "txtbxHistoryMessage";
            this.txtbxHistoryMessage.ReadOnly = true;
            this.txtbxHistoryMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtbxHistoryMessage.Size = new System.Drawing.Size(649, 406);
            this.txtbxHistoryMessage.TabIndex = 1;
            this.txtbxHistoryMessage.TextChanged += new System.EventHandler(this.txtbxHistoryMessage_TextChanged);
            // 
            // lstbxHistory
            // 
            this.lstbxHistory.FormattingEnabled = true;
            this.lstbxHistory.ItemHeight = 15;
            this.lstbxHistory.Location = new System.Drawing.Point(8, 36);
            this.lstbxHistory.Name = "lstbxHistory";
            this.lstbxHistory.ScrollAlwaysVisible = true;
            this.lstbxHistory.Size = new System.Drawing.Size(120, 409);
            this.lstbxHistory.TabIndex = 0;
            this.lstbxHistory.SelectedIndexChanged += new System.EventHandler(this.lstbxHistory_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 478);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Truckler Sample App";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label label1;
        private Button btnConnect;
        private GroupBox groupBox1;
        private Label lblConnected;
        private Label lblSteamId;
        private CheckBox chkbxConnected;
        private TextBox txtbxLog;
        private GroupBox groupBox2;
        private TextBox txtbxSteamID;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private Panel panel1;
        private TabPage tabPage2;
        private ListBox lstbxHistory;
        private TextBox txtbxHistoryMessage;
        private TextBox txtbxDiscordSnowflake;
        private Label lblDiscordSnowflake;
        private Label label3;
        private Label label2;
        private Button btnGetTrailers;
        private Button btnGetTruck;
        private Button btnGetJob;
    }
}