﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Truckler.Messages;
namespace TrucklerSampleApp
{
    public class TrucklerMessage
    {
        public TrucklerMessage()
        {
            Display = "";
        }

        public TrucklerMessage(string display, BaseMessage message)
        {
            Display = display;
            Message = message;
        }

        public string Display { get; set; }
        public Truckler.Messages.BaseMessage Message { get; set; }
    }
}
