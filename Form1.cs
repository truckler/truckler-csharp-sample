using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Text.Json;

namespace TrucklerSampleApp
{
    public partial class Form1 : Form
    {
        //Declare the client variable
        Truckler.Client client;

   


        public Form1()
        {
            InitializeComponent();

           

          
            lstbxHistory.DisplayMember = "Display";
            lstbxHistory.ValueMember = "Message";
           

            //initialize the Truckler Client
            client = new Truckler.Client();

            //register event handlers for the Truckler Client

            //client connected event
            client.Connected += Client_Connected;

            //client disconnected event
            client.Disconnected += Client_Disconnected;

            //client error event
            client.Error += Client_Error;

            //server accepted connection event
            client.ServerAccepted += Client_ServerAccepted;

            //server denied connection event
            client.ServerDenied += Client_ServerDenied;

            //server shutdown event
            client.ServerShutdown += Client_ServerShutdown;
            
            //client player info received evnet
            client.TrucklerPlayerInfo += Client_TrucklerPlayerInfo;

            //client gameplay event
            client.TrucklerGameplayEvent += Client_TrucklerGameplayEvent;

            //client job configuration event
            client.SDKConfigurationJob += Client_SDKConfigurationJob;

            //client truck configuration event
            client.SDKConfigurationTruck += Client_SDKConfigurationTruck;

            //client trailer configuration event
            client.SDKConfigurationTrailer += Client_SDKConfigurationTrailer;

            client.UnknownMessageReceived += Client_UnknownMessageReceived;
            
            
        }

        private void Client_UnknownMessageReceived(Truckler.Messages.Unknown message)
        {
            LogMessage("Unkown");
            AddMessageToHistory(new TrucklerMessage("Unkown", message));
        }

        private void Client_SDKConfigurationTrailer(Truckler.Messages.SDKConfigurationTrailer message)
        {
            LogMessage("SDK Trailer Configuration");
            AddMessageToHistory(new TrucklerMessage("SDK Trailer Configuration", message));
        }

        private void Client_SDKConfigurationTruck(Truckler.Messages.SDKConfigurationTruck message)
        {
            LogMessage("SDK Truck Configuration");
            AddMessageToHistory(new TrucklerMessage("SDK Truck Configuration", message));
        }

        private void Client_SDKConfigurationJob(Truckler.Messages.SDKConfigurationJob message)
        {
            LogMessage("SDK Job Configuration");
            AddMessageToHistory(new TrucklerMessage("SDK Job Configuration", message));
        }

        private void Client_TrucklerGameplayEvent(Truckler.Messages.TrucklerGameplayEvent message)
        {
            LogMessage("Gameplay Event: " + message.EventId);
            AddMessageToHistory(new TrucklerMessage(message.EventId, message));
        }

        private void Client_TrucklerPlayerInfo(Truckler.Messages.TrucklerPlayerInfo message)
        {
            LogMessage("Player Info Received");
            AddMessageToHistory(new TrucklerMessage("Player Info", message));

            //let's update the textbox with the player's info:
            txtbxSteamID.Invoke(() => {
                txtbxSteamID.Text = message.SteamUser.SteamId;
            });

            //let's update the discord textbox
            txtbxDiscordSnowflake.Invoke(() =>
            {
                txtbxDiscordSnowflake.Text = message.DiscordUser.SnowflakeId;
            });


        }

        private void Client_ServerShutdown(Truckler.Messages.ServerShutdown serverShutdownMessage)
        {
            LogMessage("Server is shutting down");
            AddMessageToHistory(new TrucklerMessage("Shutdown", serverShutdownMessage));
        }

        private void Client_ServerDenied(Truckler.Messages.ServerDeny serverDenyMessage)
        {
            LogMessage("Server denied the connection");
        }

        private void Client_ServerAccepted(Truckler.Messages.ServerAccept serverAcceptMessage)
        {
            LogMessage("Server accepted the connection - connection id: " + serverAcceptMessage.ClientId);
            AddMessageToHistory(new TrucklerMessage("Server Accepted", serverAcceptMessage));

            //enable the get buttons
            SetGetButtonsEnabled(true);

            //signup for truckler events:
            client.SendMessage(Truckler.Messages.ClientSubscribeEvents.Build());

            //our connection is accepted. let's request the player info.
            client.SendMessage(Truckler.Messages.ClientRequestPlayerInfo.Build());


        }

        private void Client_Error(Truckler.ClientEventDetails e)
        {
            LogMessage("Client Error: " + e.Exception.Message.ToString());
        }

        private void Client_Disconnected(Truckler.ClientEventDetails e)
        {
            SetupConnectButton(true, "Connect");
            LogMessage("Disconnected");

            //update the checkbox
            chkbxConnected.Invoke(() =>
            {
                chkbxConnected.Checked = false;
            });

            //disable the get buttons
            SetGetButtonsEnabled(false);
        }

        private void Client_Connected(Truckler.ClientEventDetails e)
        {
            SetupConnectButton(true, "Disconnect");
            LogMessage("Connected");

            //update the checkbox
            chkbxConnected.Invoke(() =>
            {
                chkbxConnected.Checked = true;
            });

        }


        /// <summary>
        /// This function is the click callback for the connect button. This connects/disconnects the client depending on the state of the client.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConnect_Click(object sender, EventArgs e)
        {
            btnConnect.Enabled = false;
            if(client.IsConnected())
            {
                //try/catch is used to handle exceptions
                try
                {
                    client.Disconnect();
                }
                //handle exception
                catch(Exception ex)
                {
                    MessageBox.Show("Exception: " + ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //don't update the text here...wait for the event handler
            }
            else
            {
               
                try
                {
                    //connect with default settings.
                    client.ConnectDefault();
                }
                //handle exception
                catch (Exception ex)
                {
                    MessageBox.Show("Exception: " + ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //don't update the button text here...wait for the event handler
            }
        }


        /// <summary>
        /// This function changes the properties of the connect/disconnect button and is thread-safe.
        /// In c# winforms, only the main thread is allowed to modify button properties. 
        /// Using the dispatcher allows us to make modifications from a separate thread.
        /// This is important because the truckler client runs on a different thread to prevent UI-blocking.
        /// The text parameter is optional and not required. Defaulting the value to an empty string.
        /// </summary>
        /// <param name="enabled"></param>
        /// <param name="text"></param>
        private void SetupConnectButton(bool enabled, string text="")
        {
            //use invoke to "interrupt" the GUI thread
            btnConnect.Invoke(() =>
            {
                btnConnect.Enabled = enabled;
                //make the text optional - if it's empty then don't update the button text
                if (!String.IsNullOrEmpty(text))
                {
                    btnConnect.Text = text;
                }
            });
        }

        /// <summary>
        /// Append a message to the log text box
        /// </summary>
        /// <param name="message"></param>
        private void LogMessage(string message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("[{0}] [{1}]  ", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString()));
            sb.Append(message);
            sb.Append(Environment.NewLine);

            txtbxLog.Invoke(() =>
            {
                //automatically clear the log box if the character count gets to 30,000
                if(txtbxLog.Text.Length > 30000)
                {
                    txtbxLog.Text = "";
                }
                txtbxLog.AppendText(sb.ToString());
            });
        }

        private void lstbxHistory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ignore if the selection was cleared
            if (lstbxHistory.SelectedIndex < 0)
                return;

            Truckler.Messages.BaseMessage basemsg = ((TrucklerMessage)lstbxHistory.Items[lstbxHistory.SelectedIndex]).Message;
            string message = System.Text.Encoding.UTF8.GetString(basemsg.Raw);
            txtbxHistoryMessage.Text = message;
        }


        /// <summary>
        /// Called when the text changes on the history text box so we can pretty-print the JSON structure.
        /// The JSON is already parsed into the data structure of the callback handlers.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtbxHistoryMessage_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!txtbxHistoryMessage.Text.Contains("}") || !txtbxHistoryMessage.Text.Contains("}"))
                    return;

                int beginJson = txtbxHistoryMessage.Text.IndexOf("{");
                int endJson = txtbxHistoryMessage.Text.LastIndexOf("}");
                int jsonLength = endJson - beginJson;
                string header = txtbxHistoryMessage.Text.Substring(0,beginJson);
                string json = txtbxHistoryMessage.Text.Substring(beginJson);
                using (var jDoc = JsonDocument.Parse(json))
                {
                    txtbxHistoryMessage.Text = header + JsonSerializer.Serialize(jDoc, new JsonSerializerOptions { WriteIndented = true });
                }
            }
            catch(Exception ex)
            {

            }
        }
        /// <summary>
        /// This enables/disables the get buttons for job, truck, and trailer configurations
        /// </summary>
        /// <param name="enabled"></param>
        private void SetGetButtonsEnabled(bool enabled)
        {
            //let's use the form invoke since we're updating multiple controls at the same time
            this.Invoke(() =>
            {
                btnGetJob.Enabled = enabled;
                btnGetTrailers.Enabled = enabled;
                btnGetTruck.Enabled = enabled;
            });
        }

        /// <summary>
        /// Called when the Get Job button is clicked.
        /// This sends a request to the Truckler plugin to send the job configuration on the next frame
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetJob_Click(object sender, EventArgs e)
        {
            if(client.IsConnected())
            {
                client.SendMessage(Truckler.Messages.ClientRequestJobConfiguration.Build());
            }
        }

        /// <summary>
        /// Called when the Get Truck button is clicked.
        /// This sends a request to the Truckler plugin to send the truck configuration on the next frame.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetTruck_Click(object sender, EventArgs e)
        {
            if (client.IsConnected())
            {
                client.SendMessage(Truckler.Messages.ClientRequestTruckConfiguration.Build());
            }
        }

        /// <summary>
        /// Called when the Get Trailers button is clicked.
        /// This sends a request to the Truckler plugin to send the trailer configuration on the next frame.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGetTrailers_Click(object sender, EventArgs e)
        {
            if (client.IsConnected())
            {
                client.SendMessage(Truckler.Messages.ClientRequestTrailerConfiguration.Build());
            }
        }
        /// <summary>
        /// Add a message to the history list box
        /// </summary>
        /// <param name="message"></param>
        private void AddMessageToHistory(TrucklerMessage message)
        {
            lstbxHistory.Invoke(() =>
            {
                lstbxHistory.Items.Add(message);
            });
        }
    }
}